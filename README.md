

# terraform-tfe-sentinel-core

Terraform module designed to generate a resource for policy set to be assigned sandbox accounts. When new accounts for sandbox are being vended the following module will be consumed and respective policy set will be associated.


## Usage

### Simple Example : Module will be consumed as 
```hcl
module "sentinel_policies" {
  source = "git::https://bitbucket.prod.tlz.dev/scm/tlz-pmr/terraform-tfe-sentinel-sandbox.git""
  hostname = "ptfe.prod.tlz.dev"
  organization = "ResMed"
  workspace = "name of sandbox workspace"
  policies_path = "${var.tfe_policies_path_prod}"
  vcs_identifier = "TLZ-PMR/terraform-tfe-sentinel-sandbox"
  oauth_token_id = "${var.tfe_oauth_token_id}"
}
```
