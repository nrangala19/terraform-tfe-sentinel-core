
data "tfe_workspace_ids" "all" {
  names        = ["*"]
  organization = "${var.organization}"
}

locals {
  workspaces = "${data.tfe_workspace_ids.all.external_ids}"
}


resource "tfe_policy_set" "baseline" {
  name                   = "${var.workspace}"
  description            = "A brand new policy set"
  organization           = "${var.organization}"
  policies_path          = "${var.policies_path}"
  workspace_external_ids =  ["${local.workspaces["${var.workspace}"]}",]

  vcs_repo {
    identifier         = "${var.vcs_identifier}"
    branch             = "master"
    ingress_submodules = false
    oauth_token_id     = "${var.oauth_token_id}"
  }
}