# variable "token" {}

variable "organization" {
  description = "The TFE organization to apply your changes to."
}

variable "workspace" {
  description = "The TFE workspace to apply your changes to."
}

variable "policies_path" {
  description = "Folder under the repo where policies.sentinel file exists"
}

variable "vcs_identifier" {
  description = "Name of the repo where sentinel.hcl file exists"
}

variable "oauth_token_id" {
  description = "OAuth token id for the repo"
}
